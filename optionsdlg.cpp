#include "optionsdlg.h"
#include "ui_optionsdlg.h"

optionsDlg::optionsDlg(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::optionsDlg)
{
    ui->setupUi(this);
    ui->progressBar->setVisible(false);
    connect(ui->listWidget,SIGNAL(itemDoubleClicked(QListWidgetItem*)),SLOT(doubleclick()));

    acciones = NULL;

    lowIRBtn = new QColorButton(QColor(78,78,78),ui->groupBox_12);
    lowIRBtn->setGeometry(30,50,30,30);

    highIRBtn = new QColorButton(QColor(255,255,255),ui->groupBox_12);
    highIRBtn->setGeometry(105,50,30,30);

    ui->tabWidget->setCurrentIndex(0);

    //---> Ponemos los métodos para el testLab
    ui->methodsCbx->addItem("Otsu+Binary");
    ui->methodsCbx->addItem("Otsu+Binary combined Binary");
    ui->methodsCbx->addItem("Binary 48");
    ui->methodsCbx->addItem("Binary 105");
    ui->methodsCbx->addItem("Binary 122");
    ui->methodsCbx->addItem("Adaptative");
    ui->methodsCbx->addItem("Combined OtsuB48+105");
    ui->methodsCbx->addItem("Combined OtsuB48+105+48");
    ui->methodsCbx->addItem("Combined OtsuB48+105+122");
    ui->methodsCbx->addItem("Combined OtsuB48+105+122+48");
}

void optionsDlg::setCommandsList(QList<commandsStructure> *atc)
{
    acciones = atc;
}

optionsDlg::~optionsDlg()
{
    delete ui;
}

void optionsDlg::hideProgressBar()
{
    ui->progressBar->setVisible(false);
}

void optionsDlg::on_grayBtn_clicked()
{
    emit toGrayImage();
}

void optionsDlg::on_contourBtn_clicked()
{
    emit calcContours();
}

void optionsDlg::on_openBtn_clicked()
{
    if (!ui->fileTxt->text().isEmpty() && QFile::exists(ui->fileTxt->text()))
    {
        emit loadImage(ui->fileTxt->text());
    }
    else if (ui->fileTxt->text().isEmpty())
    {
        QMessageBox::warning(this,"Escribe la dirección de un archivo","No hay archivo que abrir");
    }
    else
    {
        QMessageBox::warning(this,"Archivo inválido","El archivo no existe:\n " + ui->fileTxt->text());
    }
}

void optionsDlg::on_applyGBtn_clicked()
{
    int size = ui->sizeGSB->value();
    double sigma = ui->sigmaGSB->value();
    int type = ui->borderGCB->currentIndex();
    ui->progressBar->setVisible(true);
    emit toGaussianBlurEffect(size,sigma,type);
}

void optionsDlg::on_applyMBtn_clicked()
{
    int size = ui->sizeMSB->value();
    ui->progressBar->setVisible(true);
    emit toMedianBlurEffect(size);
}

void optionsDlg::on_applyNBtn_clicked()
{
    int size = ui->sizeNSB->value();
    double anchor = ui->anchorNSB->value();
    int type = ui->borderNCB->currentIndex();
    ui->progressBar->setVisible(true);
    emit toBlurEffect(size,anchor,type);
}

void optionsDlg::on_applyBFBtn_clicked()
{
    int ddpeth = ui->ddepthBFSB->value();
    int size = ui->sizeNSB->value();
    double anchor = ui->anchorNSB->value();
    int type = ui->borderNCB->currentIndex();
    bool normal = ui->normalizarBFCbx->isChecked();
    ui->progressBar->setVisible(true);
    emit toBoxFilterEffect(ddpeth,size,anchor,normal,type);
}

void optionsDlg::on_appleNTBtn_clicked()
{
    double thresh = ui->threshNTSB->value();
    double max = ui->maxNTSB->value();
    int type = ui->typeNTCB->currentIndex() + ui->type2NTCB->currentIndex();

    ui->progressBar->setVisible(true);

    emit calcThreshold(thresh,max,type);
}

void optionsDlg::on_appleATBtn_clicked()
{
    double max = ui->maxATSB->value();
    int method = ui->methodATCB->currentIndex();
    int type = ui->typeATCB->currentIndex();
    int blocksize = ui->blackSizeATSB->value();
    double c = ui->cATSB->value();

    ui->progressBar->setVisible(true);

    emit calcAdaptativeThreshold(max, method, type, blocksize, c);
}

void optionsDlg::on_applyBCBtn_clicked()
{
    double contrast = ui->contrastLSld->value() / 100.0;
    int brigth = ui->brigthLSld->value();

    ui->progressBar->setVisible(true);

    emit applyContrastandBrigth(contrast,brigth);
}

void optionsDlg::on_applyCBtn_clicked()
{
    double threshold1 = ui->thresh1CSB->value();
    double threshold2 = ui->thresh2CSB->value();
    int apertureSize = ui->apertureSizeCSB->value();
    bool L2gradient = ui->l2metricCCbx->isChecked();

    ui->progressBar->setVisible(true);

    emit findCornersCanny( threshold1, threshold2, apertureSize, L2gradient );
}

void optionsDlg::on_applyEVAVBtn_clicked()
{
     int blockSize = ui->sizeEVAVSB->value();
     int ksize = ui->ksizeEVAVSB->value();
     int borderType = ui->typeEVAVCB->currentIndex();

     ui->progressBar->setVisible(true);

     emit findCornersEigen( blockSize, ksize, borderType );
}

void optionsDlg::on_applyMDCBtn_clicked()
{
    float h = ui->hvalMDCSB->value();
    float hColor = ui->colorhMDCSB->value();
    int templateWindowSize = ui->templatesizeMDCSB->value();
    int searchWindowSize = ui->windowMDCSB->value();

    ui->progressBar->setVisible(true);

    denoisingNlMeansColored( h, hColor, templateWindowSize, searchWindowSize);
}

void optionsDlg::on_applyCABtn_clicked()
{
    double detalle = ui->detailCASld->value()/1000.0;

    ui->progressBar->setVisible(true);

    emit calcApproxContours(detalle);
}

void optionsDlg::on_resetBtn_clicked()
{
    emit resetToOriginal();
}

void optionsDlg::on_quitBtn_clicked()
{
    emit want2Leave();
}

void optionsDlg::on_getFileBtn_clicked()
{
    QString File =  QFileDialog::getOpenFileName(0, "Abrir Imagen", "", "Imagenes (*.png *.jpg *.bmp)");
    if (!File.isEmpty())
    {
        ui->fileTxt->setText(File);
    }
}

void optionsDlg::updateList()
{
    if (acciones != NULL)
    {
        ui->listWidget->clear();
        for(int i =0; i < acciones->size(); i++)
        {
            ui->listWidget->addItem(acciones->at(i).getMessage());
        }
    }
}

void optionsDlg::doubleclick()
{
    if (acciones != NULL && acciones->size() > 0)
    {
        int i = ui->listWidget->currentRow();
        emit setOldImage(acciones->at(i).getImage());
    }
}

void optionsDlg::on_eqHistBtn_clicked()
{
    emit equalizeHist();
}

void optionsDlg::on_applyIRBtn_clicked()
{
    emit inRange(lowIRBtn->getColor(),highIRBtn->getColor());
}

void optionsDlg::on_saveLogBtn_clicked()
{
    if (acciones!= NULL && acciones->size() > 0)
    {
        QString file = QFileDialog::getSaveFileName(this,"Crea una archivo","","Texto (*.txt)");
        if (!file.isEmpty())
        {
            QFile archivo(file);
            if (archivo.open(QIODevice::WriteOnly | QIODevice::Text))
            {
                QTextStream out(&archivo);
                for(int i =0; i < acciones->size(); i++)
                {
                    out << acciones->at(i).getMessage() << "\n";
                }
            }
        }
    }
}

void optionsDlg::on_applyMBtn_2_clicked()
{
    int op = ui->tipoMCbx->currentIndex();
    double anchor = ui->anchorMSB->value();
    int iter = ui->iterMSbx->value();
    int bordertype = ui->borderMCbx->currentIndex();

    emit morphologyEx(op, anchor, iter, bordertype);
}

void optionsDlg::on_method1Btn_clicked()
{
    emit method1();
}

void optionsDlg::on_applyHLBtn_clicked()
{
    double rho = ui->rhoHLSbx->value();
    double theta = ui->thetaHLSbx->value();
    int threshold = ui->threshHLPSB->value();
    double minLineLength = ui->minlineHLSbx->value();
    double maxLineGap = ui->maxlineHLSbx->value();

    emit HoughLinesP(rho, theta, threshold, minLineLength, maxLineGap);
}

void optionsDlg::on_applySobelBtn_clicked()
{
    int ddepth = ui->ddepthSobelCbx->currentIndex() == 0 ? -1 : CV_64F;
    int dx = ui->dxSobelSbx->value();
    int dy = ui->dySobelSbx->value();
    int ksize = ui->ksizeSobelSbx->value();
    double scale = ui->scaleSobelSbx->value();
    double delta = ui->deltaSobelSbx->value();
    int borderType = ui->bordertSobelCbx->currentIndex();

    emit Sobel(ddepth, dx, dy, ksize, scale, delta, borderType);
}

void optionsDlg::on_applyScharrBtn_clicked()
{
    int ddepth = ui->ddepthScharrCbx->currentIndex() == 0 ? -1 : CV_64F;
    int dx = ui->dxScharrSbx->value();
    int dy = ui->dyScharrSbx->value();
    double scale = ui->scaleScharrSbx->value();
    double delta = ui->deltaScharrSbx->value();
    int borderType = ui->bordertScharrCbx->currentIndex();

    emit Scharr(ddepth, dx, dy, scale, delta, borderType);
}

void optionsDlg::on_SobelCombBtn_clicked()
{
    emit SobelCombined();
}

void optionsDlg::on_pushButton_clicked()
{
    emit method2();
}

void optionsDlg::on_pushButton_2_clicked()
{
    emit method3();
}

void optionsDlg::on_usingmehtod1Btn_clicked()
{
    emit UseMethod1();
}

void optionsDlg::on_thresholdCombBtn_clicked()
{
    emit combThreshold();
}

void optionsDlg::on_runBtn_clicked()
{
    if (QFileInfo::exists(ui->getFolderInBtn->text()) && QFileInfo::exists(ui->setFolderOutBtn->text()) && ui->methodsCbx->currentIndex() > 0)
    {
        ui->methodsCbx->setEnabled(false);
        ui->getFolderInBtn->setEnabled(false);
        ui->setFolderOutBtn->setEnabled(false);
        ui->runBtn->setEnabled(false);
        ui->progressBar->setVisible(true);
        ui->progressBar->setTextVisible(true);
        ui->infoLbl->setText("Empezando...");
        ui->logMethodsTxt->clear();

        hilo = new TestLabThread(this);
        connect(hilo,SIGNAL(finalizado()),SLOT(finalizadoLab()));
        connect(hilo,SIGNAL(infoFail(QString)),SLOT(infoLabFail(QString)));
        connect(hilo,SIGNAL(updateInfo(int,int,int)),SLOT(updateLabInfo(int,int,int)));

        hilo->analyseFolder(ui->getFolderInBtn->text(),ui->setFolderOutBtn->text(),ui->methodsCbx->currentIndex());
    }
    else
    {
        QMessageBox::warning(this,"No","Completa el formulario");
    }
}

void optionsDlg::updateLabInfo(int exito, int fallo, int total )
{
    ui->progressBar->setMaximum(total);
    ui->infoLbl->setText("buenas: " + QString::number(exito) + "  malo: " + QString::number(fallo) + " porcentaje de exito: " + QString::number(((double)exito)/((double)total)*100) );
    ui->progressBar->setValue(exito+fallo);
}

void optionsDlg::infoLabFail(QString msg)
{
    ui->logMethodsTxt->addItem(msg);
}

void optionsDlg::finalizadoLab()
{
    hideProgressBar();
    ui->progressBar->setValue(0);
    ui->progressBar->setMaximum(0);
    ui->progressBar->setTextVisible(false);
    delete hilo;
    ui->methodsCbx->setEnabled(true);
    ui->getFolderInBtn->setEnabled(true);
    ui->setFolderOutBtn->setEnabled(true);
    ui->runBtn->setEnabled(true);
}

void optionsDlg::on_getFolderInBtn_clicked()
{
    QString Folder =  QFileDialog::getExistingDirectory(0, "Elije una carpeta con imágenes");
    if (!Folder.isEmpty())
    {
        ui->getFolderInBtn->setText(Folder);
    }
}

void optionsDlg::on_setFolderOutBtn_clicked()
{
    QString Folder =  QFileDialog::getExistingDirectory(0, "Elije una carpeta para guardar la salida");
    if (!Folder.isEmpty())
    {
        ui->setFolderOutBtn->setText(Folder);
    }
}

void optionsDlg::on_meansBtn_clicked()
{
    emit promedios();
}

void optionsDlg::on_usingFinalMethodBtn_clicked()
{
    emit UserMethodCombined();
}
