#ifndef VENTANA_H
#define VENTANA_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions_2_0>
#include <QKeyEvent>
#include <QFileDialog>
#include <QString>
#include <QTime>
#include <opencv2/core/core.hpp>
#include <opencv2/opencv.hpp>
#include <cmath>
#include <QList>

#include "comandsstructure.h"


class ventana : public QOpenGLWidget, protected QOpenGLFunctions_2_0
{
    Q_OBJECT

public:
    ventana(QWidget *parent = 0);
    ~ventana();

public slots:
    void GrayImage();
    void GaussianBlur(int size,double sigma, int type);
    void MedianBlur(int size);
    void Blur(int size,double anchor, int type);
    void BoxFilter(int ddepth,int size,double anchor, bool normalize, int type);
    bool loadImage(const cv::Mat& image);
    void getContours();
    void getApproxContours(double detail);
    void BrightnessContrast(double contrast,int bright);
    void InRange(QColor low, QColor high);
    void Thresholding(double thresh, double max, int type);
    void AdaptativeThresholding(double max, int method, int type, int blocksize, double c);
    void Canny(double threshold1, double threshold2, int apertureSize, bool L2gradient);
    void cornerEigenValsAndVecs(int blockSize, int ksize, int borderType);
    void denoisingNlMeansColored(float h, float hColor, int templateWindowSize, int searchWindowSize);
    void equalizeHist();
    void back2Source();
    void morphologyEx(int op, double anchor, int iter, int bordertype);
    void method1();
    void method2();
    void method3();
    void UseMethod1();
    void UseMethodsCombined();
    void combineThreshold();
    void HoughLinesP(double rho, double theta, int threshold, double minLineLength, double maxLineGap);
    void Sobel(int ddepth, int dx, int dy, int ksize, double scale, double delta, int borderType);
    void Scharr(int ddepth, int dx, int dy, double scale, double delta, int borderType);
    void SobelCombined();
    void meanTest();

    bool showImage(const cv::Mat& image); /// Used to set the image to be viewed

signals:
    void imageSizeChanged( int outW, int outH ); /// Used to resize the image outside the widget
    void changeImage(commandsStructure nueva);

protected:
    void initializeGL(); /// OpenGL initialization
    void paintGL(); /// OpenGL Rendering
    void resizeGL(int width, int height);        /// Widget Resize Event

    void updateScene();
    void renderImage();
    void saveImg(cv::Mat img,QString name);
    //void keyPressEvent(QKeyEvent *event);


private:

    QImage mRenderQtImg;           /// Qt image to be rendered
    QImage mResizedImg;
    cv::Mat mCurrentImage;             /// OpenCV image to be shown
    cv::Mat mSrcImage;             /// original OpenCV image to be shown
    cv::Mat mRenderImage;

    QColor mBgColor;		/// Background color

    float mImgRatio;             /// height/width ratio

    int mRenderWidth;
    int mRenderHeight;
    int mRenderPosX;
    int mRenderPosY;

    void recalculatePosition();
    cv::Scalar LinearGradient(cv::Scalar A, cv::Scalar B, float pos);
    int bordes(int);
    int threshtypes(int);
    QString Color2String(QColor);

    bool stdDevisOK(cv::Scalar standard);
    bool meanisOK(cv::Scalar media);
    bool almostSquare(std::vector<cv::Point> quad);
    cv::Point getCenter(std::vector<cv::Point> quad);
    std::vector<cv::Point> orderCenters(cv::Point c1, cv::Point cw, cv::Point c3);
    double calcAngleBetween(cv::Point a, cv::Point b, cv::Point c);
    cv::Point complementaryPoint(cv::Point a, cv::Point b, cv::Point c);
    cv::Point growPoint(double grow, cv::Point v, cv::Point w, cv::Point p);
    std::vector<cv::Point> calcularActa(double grow, cv::Point a, cv::Point b, cv::Point c, cv::Point d);
    void calcTransformationsPoints(cv::Point2f *vert, std::vector<cv::Point> centros, cv::Point2f *src, cv::Point2f *dest, int size);
    std::vector<cv::Point> reCalculateCenters(std::vector<cv::Point> c, cv::Mat m);
    std::vector<std::vector<cv::Point>> reCalculateSquares(std::vector<std::vector<cv::Point>> c, cv::Mat m);
    std::vector<cv::Point> reCalculateCentersP(std::vector<cv::Point> c, cv::Mat m);
    std::vector<std::vector<cv::Point>> reCalculateSquaresP(std::vector<std::vector<cv::Point>> c, cv::Mat m);
    cv::Mat cropImageProportion(cv::Mat img,std::vector<cv::Point> centers,double proportionIniX,double proportionLongX,double  proportionIniY,double proportionLongY );
    cv::Mat previewImage(cv::Mat img, std::vector<cv::Point> centers);
    cv::Mat dataImage(cv::Mat img, std::vector<cv::Point> centers);
    cv::Mat ThresholdingCombiner(cv::Mat img1, cv::Mat img2);
    uchar combine(uchar color1, uchar color2);
    cv::Point calculatePerspectiveFourthPoint(std::vector<std::vector<cv::Point>> squares, std::vector<cv::Point> centers);
    std::vector<cv::Point> calculatePerspectiveActa(std::vector<std::vector<cv::Point>> squares, std::vector<cv::Point> &centers, cv::Mat img);
    std::vector<std::vector<cv::Point>> obtainSquares(cv::Mat imagen, cv::Mat srcimg);
    void removeDuplicateSquares(std::vector<std::vector<cv::Point>> *quads);
    int minimumStdDevSquares(std::vector<std::vector<cv::Point>> &cuadros, cv::Mat srcimg);

};

#endif // VENTANA_H
