#ifndef COMANDSSTRUCTURE_H
#define COMANDSSTRUCTURE_H

#include <opencv2/core/core.hpp>
#include <QString>

class commandsStructure
{

public:

    commandsStructure(){}
    commandsStructure(QString message, cv::Mat image){ mensaje = message; imagen = image; }

    void setMessage(QString msg){mensaje = msg;}
    QString getMessage() const{return mensaje;}
    void setImage(cv::Mat image){imagen = image;}
    cv::Mat getImage() const{return imagen;}
private:
    cv::Mat imagen;
    QString mensaje;
};

#endif // COMANDSSTRUCTURE_H
