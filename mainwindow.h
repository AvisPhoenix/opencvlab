#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QList>
#include <QString>
#include <QDockWidget>
#include "comandsstructure.h"
#include "optionsdlg.h"
#include "ventana.h"
#include <opencv2/opencv.hpp>


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
private slots:
    void openFile(QString file);
    void resetImage();
    void changeActions(commandsStructure);
private:
    optionsDlg *dock;
    ventana *opencvWindow;
    QList<commandsStructure> proceso;
};

#endif // MAINWINDOW_H
