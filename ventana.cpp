#include "ventana.h"

#include <opencv2/opencv.hpp>
#include <QDebug>

using std::vector;

const double PI = 3.141592653589793;

/** Métodos test **/
void ventana::GrayImage()
{
    cv::Mat imagen;
    cv::cvtColor(mCurrentImage,imagen, CV_BGR2GRAY);
    cv::cvtColor(imagen,imagen, CV_GRAY2BGR);
    showImage(imagen);
    qDebug()<<"Ya cambie a blanco y negro";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("2Gray");
    emit changeImage(accion);
}

void ventana::equalizeHist()
{
    cv::Mat imagen;
    cv::cvtColor(mCurrentImage,imagen, CV_BGR2GRAY);
    cv::equalizeHist(imagen,imagen);
    cv::cvtColor(imagen,imagen, CV_GRAY2BGR);
    showImage(imagen);
    qDebug()<<"Ya cambie el histograma en B/N";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("eqHist");
    emit changeImage(accion);
}

void ventana::GaussianBlur(int size,double sigma, int type)
{
    cv::Mat imagen;
    cv::GaussianBlur(mCurrentImage,imagen,cv::Size(size,size),sigma,bordes(type));
    showImage(imagen);
    qDebug()<<"Ya hice el gaussian blur";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("gassianblured-s:"+QString::number(size)+"-sigma:"+QString::number(sigma)+"-type:"+QString::number(type));
    emit changeImage(accion);
}

void ventana::MedianBlur(int size)
{
    cv::Mat imagen;
    cv::medianBlur(mCurrentImage,imagen,size);
    showImage(imagen);
    qDebug()<<"Ya hice el median blur";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("medianblured-s:"+QString::number(size));
    emit changeImage(accion);
}

void ventana::Blur(int size,double anchor, int type)
{
    cv::Mat imagen;
    cv::blur(mCurrentImage,imagen,cv::Size(size,size),cv::Point(anchor,anchor),bordes(type));
    showImage(imagen);
    qDebug()<<"Ya hice el blur";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("blurred-s:"+QString::number(size)+"-anchoir:"+QString::number(anchor)+"-type:"+QString::number(type));
    emit changeImage(accion);
}

void ventana::BoxFilter(int ddepth, int size,double anchor, bool normalize, int type)
{
    cv::Mat imagen;
    cv::boxFilter(mCurrentImage,imagen,ddepth,cv::Size(size,size),cv::Point(anchor,anchor),normalize,bordes(type));
    showImage(imagen);
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("boxfilter-dd:"+QString::number(ddepth)+"-s:"+QString::number(size)+"-a:"+QString::number(anchor)+
                      +"-n:"+QString::number(normalize)+"-t:"+QString::number(type));
    emit changeImage(accion);
    qDebug()<<"Ya hice el blur";
}

void ventana::BrightnessContrast(double contrast,int bright)
{
    cv::Mat imagen;
    mCurrentImage.convertTo(imagen,-1,contrast,bright);
    showImage(imagen);
    qDebug()<<"Ya hice el Brillo  y contraste";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("BandC-b:"+QString::number(bright) + "-c:"+QString::number(contrast));
    emit changeImage(accion);
}

void ventana::InRange(QColor low, QColor high )
{
    cv::Mat imagen;
    cv::cvtColor( mCurrentImage, imagen, CV_BGR2HSV );
    cv::inRange(imagen,cv::Scalar(low.blue(),low.green(),low.red()),cv::Scalar(high.blue(),high.green(),high.red()),imagen);
    cv::cvtColor( imagen, imagen, CV_GRAY2BGR );
    showImage(imagen);
    qDebug()<<"Ya hice el in range";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("inRange-low:"+Color2String(low)+"-high:"+Color2String(high));
    emit changeImage(accion);
}

void ventana::Thresholding(double thresh, double max, int type)
{
    cv::Mat imagen;
    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );
    cv::threshold(imagen,imagen,thresh,max,threshtypes(type));
    cv::cvtColor( imagen, imagen, CV_GRAY2BGR );
    showImage(imagen);
    qDebug()<<"Ya hice el thresholding";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("threshold-thresh:"+QString::number(thresh)+"-max:"+QString::number(max)+"-type:"+QString::number(type));
    emit changeImage(accion);
}

void ventana::AdaptativeThresholding(double max, int method, int type, int blocksize, double c)
{
    cv::Mat imagen;
    int metodo;

    if (method == 0)
        metodo = cv::ADAPTIVE_THRESH_MEAN_C;
    else
        metodo = cv::ADAPTIVE_THRESH_GAUSSIAN_C;

    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );
    cv::adaptiveThreshold(imagen, imagen, max, metodo, threshtypes(type),blocksize,c);
    cv::cvtColor( imagen, imagen, CV_GRAY2BGR );
    showImage(imagen);
    qDebug()<<"Ya hice el thresholding";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("adapthreshold-max:"+QString::number(max)+"-method:"+QString::number(method)+"-t:"+QString::number(type)+"-bz:"+QString::number(blocksize)+"-c:"+QString::number(c));
    emit changeImage(accion);
}

void ventana::Canny( double threshold1, double threshold2, int apertureSize, bool L2gradient)
{
    cv::Mat imagen;
    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );
    cv::Canny( imagen, imagen, threshold1, threshold2, apertureSize, L2gradient );
    cv::cvtColor( imagen, imagen, CV_GRAY2BGR );
    showImage(imagen);
    qDebug()<<"Ya hice el canny";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("canny-th1:"+QString::number(threshold1)+"-th2:"+QString::number(threshold2)+"-az:"+QString::number(apertureSize)+"-lg:"+QString::number(L2gradient));
    emit changeImage(accion);
}

void ventana::cornerEigenValsAndVecs(int blockSize, int ksize, int borderType)
{
    cv::Mat imagen;
    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );
    cv::cornerEigenValsAndVecs(imagen,imagen,blockSize,ksize,bordes(borderType));
    cv::cvtColor( imagen, imagen, CV_GRAY2BGR );
    showImage(imagen);
    qDebug()<<"Ya hice el corner";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("cEVaV-bz:"+QString::number(blockSize)+"-kz:"+QString::number(ksize)+"-t:"+QString::number(borderType));
    emit changeImage(accion);
}

void ventana::denoisingNlMeansColored(float h, float hColor, int templateWindowSize, int searchWindowSize)
{
    cv::Mat imagen;
    //Este está en photo opencv
    cv::fastNlMeansDenoisingColored(mCurrentImage,imagen, h, hColor, templateWindowSize, searchWindowSize);
    showImage(imagen);
    qDebug()<<"Ya hice quité el ruido";
    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("denoiseNLMC-h:"+QString::number(h)+"-hC:"+QString::number(hColor)+"-tWS:"+QString::number(templateWindowSize)+"-sWS:"+QString::number(searchWindowSize));
    emit changeImage(accion);
}

void ventana::getContours()
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx;
    vector<vector<cv::Point>> tempList;
    vector<cv::Vec4i> hierarchy;
    cv::Mat canvas, processImg;
    cv::Scalar color, tempColor;
    int i;
    QList<int> contornos;

    cv::cvtColor( mCurrentImage, processImg, CV_BGR2GRAY );

    cv::findContours(processImg,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    for(i=0;i < contours.size(); i++)
    {
        contornos.append(i);
    }
    qDebug()<<"Contornos: " << contornos.size();

    canvas = cv::Mat::zeros( mSrcImage.size(), CV_8UC3 );
    while (contornos.size() > 0)
    {
        color= cv::Scalar( std::rand()%255, std::rand()%128, std::rand()%255 );
        for(i=contornos.first(); i >=0; i = hierarchy[i][0])
        {
            cv::drawContours( canvas, contours, i, color, 2, 8, hierarchy, 0, cv::Point() );
            contornos.removeOne(i);
        }
    }

    showImage(canvas);

    commandsStructure accion;
    accion.setImage(canvas);
    accion.setMessage("getContours");
    emit changeImage(accion);
}

void ventana::meanTest()
{
    vector<vector<cv::Point>> contours;
    vector<vector<cv::Point>> tempList;
    vector<cv::Vec4i> hierarchy;
    cv::Mat canvas, processImg,mask, element;
    cv::Scalar cmean, stddev;
    int i;
    QList<int> contornos;
    commandsStructure accion;

    cv::cvtColor( mCurrentImage, processImg, CV_BGR2GRAY );

    cv::threshold(processImg,processImg,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);

    cv::findContours(processImg,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    for(i=0;i < contours.size(); i++)
    {
        contornos.append(i);
    }
    qDebug()<<"Contornos: " << contornos.size();

    canvas = cv::Mat::zeros( mSrcImage.size(), mSrcImage.type() );
    while (contornos.size() > 0)
    {
        for(i=contornos.first(); i >=0; i = hierarchy[i][0])
        {
            tempList.clear();
            tempList.push_back(contours.at(i));
            mask = cv::Mat::zeros( mSrcImage.size(), CV_8U );
            saveImg(mask,"Pre-Mascara" + QString::number(i) + ".png");
            cv::drawContours(mask,tempList, 0, 255, -1);
            element = cv::getStructuringElement(0,cv::Size(6,6));
            cv::morphologyEx(mask,mask,cv::MORPH_ERODE,element);
            saveImg(mask,"Mascara" + QString::number(i) + ".png");
            cmean = cv::mean(mSrcImage,mask);
            cv::drawContours(canvas,tempList, 0, cmean, -1);
            saveImg(canvas,"Paso" + QString::number(i) + ".png");
            cv::meanStdDev(mSrcImage,cmean,stddev,mask);
            qDebug() << i << "stdev= (" << stddev[0] << "," << stddev[1] << "," << stddev[2] << ")";
            qDebug() << i << "mean= (" << cmean[0] << "," << cmean[1] << "," << cmean[2] << ")";
            contornos.removeOne(i);
        }
    }

    showImage(canvas);

    accion.setImage(canvas);
    accion.setMessage("promedio");
    emit changeImage(accion);
}

void ventana::getApproxContours(double detail)
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx;
    vector<vector<cv::Point>> tempList;
    vector<cv::Vec4i> hierarchy;
    cv::Mat canvas, processImg;
    cv::Scalar color, tempColor;
    int i;
    QList<int> contornos;

    cv::cvtColor( mCurrentImage, processImg, CV_BGR2GRAY );

    cv::findContours(processImg,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    for(i=0;i < contours.size(); i++)
    {
        contornos.append(i);
    }
    qDebug()<<"Contornos: " << contornos.size();

    canvas = cv::Mat::zeros( mSrcImage.size(), CV_8UC3 );
    while (contornos.size() > 0)
    {
        color= cv::Scalar( std::rand()%255, std::rand()%128, std::rand()%255 );
        for(i=contornos.first(); i >=0; i = hierarchy[i][0])
        {
            tempList.clear();
            cv::approxPolyDP(contours[i],approx,detail*(cv::arcLength(contours[i],true)),true);
            tempList.push_back(approx);
            if (approx.size() == 4 && cv::contourArea(approx) > 0.5){
                tempColor = color;
                color = cv::Scalar( 255, 255, 255 );
                cv::drawContours( canvas, tempList, 0, color, 2, 8, hierarchy, 0, cv::Point() );
                color = tempColor;
            }
            else
            {
                cv::drawContours( canvas, tempList, 0, color, 2, 8, hierarchy, 0, cv::Point() );
            }

            //cv::drawContours( canvas, tempList, 0, color, 2, 8, hierarchy, 0, cv::Point() );
            contornos.removeOne(i);
        }
    }

    showImage(canvas);

    commandsStructure accion;
    accion.setImage(canvas);
    accion.setMessage("getContour-d:"+QString::number(detail));
    emit changeImage(accion);
}

void ventana::morphologyEx(int op, double anchor, int iter, int bordertype)
{
    int tipoM;
    switch (op) {
    case 0:
        tipoM = cv::MORPH_ERODE;
        break;
    case 1:
        tipoM = cv::MORPH_DILATE;
        break;
    case 2:
        tipoM = cv::MORPH_OPEN;
        break;
    case 3:
        tipoM = cv::MORPH_CLOSE;
        break;
    case 4:
        tipoM = cv::MORPH_GRADIENT;
        break;
    case 5:
        tipoM = cv::MORPH_TOPHAT;
        break;
    case 6:
        tipoM = cv::MORPH_BLACKHAT;
        break;
    /*case 7:
        tipoM = cv::MORPH_HITMISS;
        break;*/
    default:
        tipoM = cv::MORPH_ERODE;
        break;
    };

    cv::Mat element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::Mat imagen;
    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );
    cv::morphologyEx(imagen,imagen,tipoM,element,cv::Point(anchor,anchor),iter,bordes(bordertype));
    cv::cvtColor(imagen,imagen,CV_GRAY2BGR);
    showImage(imagen);

    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("morphoEx-op:"+QString::number(op)+"-anchor:"+QString::number(anchor)+"-iter:"+
                      QString::number(iter)+"-bt:"+QString::number(bordertype));
    emit changeImage(accion);
}

bool ventana::stdDevisOK(cv::Scalar standard)
{
    bool res = true;
    for(int i =0; i < 3; i++)
    {
        res = res && standard[i] < 27;
    }
    qDebug() << "desviacion= (" << standard[0] << "," << standard[1] << "," << standard[2] << ")";
    /*if (res){
        qDebug() << "desviacion= (" << standard[0] << "," << standard[1] << "," << standard[2] << ")";
    }*/

    return res;
    /*qDebug() << "desv Std=" << standard[0];

    return standard[0] < 55;*/
}

bool ventana::meanisOK(cv::Scalar media)
{
    bool res = true;
    int min=255, max=0;
    for(int i =0; i < 3; i++)
    {
        if (max < media[i]) max = media[i];
        if (media[i] < min) min = media[i];
        res = res && media[i] < 128;
        qDebug() << "media= (" << media[0] << "," << media[1] << "," << media[2] << ")";
    }
    if (res)
    {
        res = res && (max - min) < 30;
        qDebug() << "distancia= " << (max - min);
    }

    //qDebug() << "media=" << media[0];

    //res = media[0] < 11;

    return res;
}

bool ventana::almostSquare(vector<cv::Point> quad)
{
    vector<cv::Point> edge;
    double size1, size2, finalsize1, tol= 0.1, ratio;
    bool ok=false;
    //Primeros lados opuestos
    edge.push_back(quad[0]);
    edge.push_back(quad[1]);
    size1 = cv::arcLength(edge,false);
    edge.clear();
    edge.push_back(quad[2]);
    edge.push_back(quad[3]);
    size2 = cv::arcLength(edge,false);
    ok = std::abs(size1-size2) < size1*tol;
    if (ok)
    {
        finalsize1 = (size1+size2);
        //Segundos lados opuestos
        edge.clear();
        edge.push_back(quad[1]);
        edge.push_back(quad[2]);
        size1 = cv::arcLength(edge,false);
        edge.clear();
        edge.push_back(quad[3]);
        edge.push_back(quad[0]);
        size2 = cv::arcLength(edge,false);
        ok = std::abs(size1-size2) < size1*tol;
        if (ok)
        {
            size2 = (size1+size2);
            ratio = finalsize1/size2; //size2 y finalsize1 eran divididos entre dos para ser el promedio, pero en esta divición se cancela el 2, por lo que no tiene sentido hacer la división
            ok = ratio > 0.8 && ratio < 1.25;
        }
    }
    return ok;
}

double ventana::calcAngleBetween(cv::Point a, cv::Point b, cv::Point c)
{
    /**
    *       Función que calcula en ángulo entre dos segmentos (a-b) y (c-b).
    *       El ángulo va desde 0 hasta 6.2657320147 radianes aprox (0-359 grados)
    *
    *       Entrada
    *           a,b,c: JSON {x,y} puntos que definen los segmentos con un punto en común (b).
    *
    *       Salida
    *           angle: Número el ángulo entre los segmentos.
    *
    *       Autor: Gustavo Adolfo García Cano
    *       email: gustavo.garcia@ine.mx
    **/
    cv::Point v, w;
    double det, prod, angle;
    v = a - b;
    w = c - b;
    angle = -1;
    if (std::abs(v.x+v.y) > 1e-12 || std::abs(w.x+w.y) > 1e-12){
        det = v.x*w.y - v.y*w.x;
        prod = w.x*v.x + v.y*w.y;
        angle = std::atan2(det,prod);
        if (std::abs(angle) > 1e-12 && angle < 0){
            angle = 2*PI + angle;
        }
    }
    return angle;
}

cv::Point ventana::getCenter(vector<cv::Point> quad)
{
    cv::Point2d centro, A1, A2, B1, B2;
    double alpha=0,tan=0, div=0;;

    A1 = quad[0];
    A2 = quad[2]-quad[0];

    B1 = quad[1];
    B2 = quad[3]-quad[1];

    if (std::abs(A2.x+A2.y) > 1e-16 || std::abs(B2.x+B2.y) > 1e-16)
    {
        if (std::abs(B2.x) > 1e-16)
        {
            tan = B2.y/B2.x;
            div = A2.y - A2.x*tan;
            if (std::abs(div) > 1e-16)
                alpha = (B1.y-A1.y - (B1.x-A1.x)*tan)/div;
            else
                alpha = -5;
        }
        else
        {
            tan = B2.x/B2.y;
            div = (A2.x - A2.y*tan);
            if (std::abs(div) > 1e-16)
                alpha = (B1.x-A1.x - (B1.y - A1.y)*tan)/div;
            else
                alpha = -5;
        }
    }

    if (alpha == -5)
    {
        qDebug() << "ERROR no encontré el centro";
    }
    else
    {
        centro = A1 + alpha*A2;
    }


    return centro;
}

std::vector<cv::Point> ventana::orderCenters(cv::Point c1, cv::Point c2, cv::Point c3)
{
    bool validos = false;
    double ladoMax, lado, m, dist,angbetween;
    int temp,itemp, Ai;
    vector<cv::Point> centros;
    cv::Point pointT;

    ladoMax= cv::norm(c1-c2);
    Ai = 0;

    lado = cv::norm(c2-c3);
    if (lado > ladoMax){
        ladoMax = lado;
        Ai = 1;
    }

    lado = cv::norm(c3-c1);
    if (lado > ladoMax){
        ladoMax = lado;
        Ai = 2;
    }

    /**
        *     Una vez que tenemos el lado más largo, este está formado por los puntos A y C
        *     Lo renombramos como: centro1 = A, centro2 = C, centro3 = B, por lo tanto
        *     centro3 será el vertice opuesto a la hipotenusa
        **/
    switch (Ai) {
    case 0:
        Ai = 2;
        break;
    case 1:
        pointT = c3;
        c3 = c1;
        c1 = pointT;
        pointT = c2;
        c2 = c1;
        c1 = pointT;
        break;
    case 2:
        pointT = c2;
        c2 = c1;
        c1 = pointT;
        pointT = c3;
        c3 = c1;
        c1 = pointT;
        break;
    }

    /// Pendiente de la hipotenusa
    m = (double)(c2.y - c1.y)/(double)(c2.x - c1.x);
    /// Distancia del vértice a la hipotenusa (con orientación)
    dist = (double)( -m*c3.x + c3.y + ( m*c2.x - c2.y ) )/std::sqrt(m*m + 1);
    /// Coseno del ángulo en B (Si es cercano a cero entonces es cercano a 90º)
    angbetween = calcAngleBetween(c1,c3, c2);
    if (
        std::abs(m) > 1e-12 && std::abs(dist) > 1e-12 && angbetween > 3.1416
       )
    {
        pointT = c2;
        c2 = c1;
        c1 = pointT;
    }
    centros.push_back(c3);
    centros.push_back(c1);
    centros.push_back(c2);

    return centros;
}

cv::Point ventana::complementaryPoint(cv::Point a, cv::Point b, cv::Point c)
{
    cv::Point v,w;
    v = b - a; w = c - a;
    return v+w+a;
}

cv::Point ventana::growPoint(double grow, cv::Point a, cv::Point b, cv::Point c)
{
    cv::Point2d p1,p2,p3, v,w, p;

    v = a-b;
    v = v*(1/cv::norm(v));
    w = a-c;
    w = w*(1/cv::norm(w));

    p = a;

    p1 = p + grow*v;
    p2 = p + grow*w;
    p3 = complementaryPoint(a,p1,p2);

    return p3;
}

vector<cv::Point> ventana::calcularActa(double grow, cv::Point a, cv::Point b, cv::Point c, cv::Point d)
{
    vector<cv::Point> acta;
    double dist;

    dist = std::max(cv::norm(a-b), cv::norm(a-c))*grow;

    acta.push_back(growPoint(dist,a,b,c));
    acta.push_back(growPoint(dist,b,a,d));
    acta.push_back(growPoint(dist,d,b,c));
    acta.push_back(growPoint(dist,c,a,d));

    return acta;
}

void ventana::calcTransformationsPoints(cv::Point2f *vert, std::vector<cv::Point> centros, cv::Point2f *src, cv::Point2f *dest, int size=3)
{
    int i=1,j=0;
    //Ordenamos las esquinas
    double min;
    cv::Point2f centro;

    for (j=0; j < size; j++)
    {
        centro = centros[j];
        min = cv::norm(vert[0] - centro);
        src[j] = vert[0];

        for(i=1; i < 4; i++)
        {
            if ( cv::norm(vert[i]-centro) < min )
            {
                min = cv::norm(vert[i]-centro);
                src[j] = vert[i];
            }
        }
    }
    dest[0] = cv::Point2f(0.0,0.0);
    dest[1] = cv::Point2f(cv::norm(src[0]-src[1]),0.0);
    dest[2] = cv::Point2f(0.0,cv::norm(src[0]-src[2]));
    if (size == 4)
    {
        dest[3] = cv::Point2f(cv::norm(src[0]-src[1]),cv::norm(src[0]-src[2]));
    }
}

std::vector<cv::Point> ventana::reCalculateCenters(std::vector<cv::Point> c, cv::Mat M)
{
    std::vector<cv::Point> centros;
    cv::Point2f p;
    int i=0, j=0;

    centros.clear();

    cv::transform(c,centros,M);

    return centros;
}

std::vector<std::vector<cv::Point>> ventana::reCalculateSquares(std::vector<std::vector<cv::Point>> c, cv::Mat M)
{
    std::vector<std::vector<cv::Point>> cuadrados;
    std::vector<cv::Point> square;
    cv::Point2f p;
    int i=0;

    cuadrados.clear();

    for(i=0; i < c.size(); i++)
    {
        square.clear();
        cv::transform(c[i],square, M);
        cuadrados.push_back(square);
    }

    return cuadrados;
}

std::vector<cv::Point> ventana::reCalculateCentersP(std::vector<cv::Point> c, cv::Mat M)
{
    std::vector<cv::Point2f> centros, cuad;
    std::vector<cv::Point> centrosf;
    int i=0;

    centros.clear();

    cuad.clear();
    for(i=0; i < c.size(); i++)
    {
        cuad.push_back(c[i]);
    }
    qDebug() << "profundida: " << (M.depth() == CV_32F || M.depth() == CV_64F);
    cv::perspectiveTransform(cuad,centros,M);
    for(i=0; i < centros.size(); i++)
    {
        centrosf.push_back(centros[i]);
    }

    return centrosf;
}

std::vector<std::vector<cv::Point>> ventana::reCalculateSquaresP(std::vector<std::vector<cv::Point>> c, cv::Mat M)
{
    std::vector<std::vector<cv::Point>> cuadrados;
    std::vector<cv::Point2f> square, cuad;
    std::vector<cv::Point> squaref;
    int i=0,j;

    cuadrados.clear();

    qDebug() << "profundida: " << (M.depth() == CV_32F || M.depth() == CV_64F);

    for(i=0; i < c.size(); i++)
    {
        square.clear();
        cuad.clear();
        for(j=0; j < c[i].size(); j++)
        {
            cuad.push_back(c[i].at(j));
        }

        cv::perspectiveTransform(cuad,square, M);
        for(j=0; j < square.size(); j++)
        {
            squaref.push_back(square.at(j));
        }
        cuadrados.push_back(squaref);
    }

    return cuadrados;
}

cv::Mat ventana::cropImageProportion(cv::Mat img,std::vector<cv::Point> centers,double proportionIniX,double proportionLongX,double  proportionIniY,double proportionLongY )
{
    cv::Point2f centro1 = centers[0], centro2 = centers[1], centro3 = centers[2];
    cv::Point2f pointIni;
    cv::Size sizeActa, sizeFinal;
    cv::Rect areaFinal;
    cv::Mat nImg;

    sizeActa.width = centro2.x - centro1.x;
    sizeActa.height = centro3.y - centro1.y;

    pointIni.x = centro1.x + proportionIniX*(double)sizeActa.width;
    pointIni.y = centro1.y + proportionIniY*(double)sizeActa.height;

    sizeFinal.width = proportionLongX*(double)sizeActa.width;
    sizeFinal.height = proportionLongY*(double)sizeActa.height;

    areaFinal = cv::Rect(pointIni,sizeFinal);

    nImg = img(areaFinal);

    return nImg;
}

cv::Mat ventana::previewImage(cv::Mat img, std::vector<cv::Point> centers)
{
    cv::Mat nimg;
    double zoom;
    nimg = cropImageProportion(img, centers, 0.395, 0.284, 0.13,  0.26);
    zoom = 800.0/(double)nimg.size().width;
    cv::resize(nimg,nimg,cv::Size(zoom*nimg.size().width,zoom*nimg.size().height),0,0,cv::INTER_LANCZOS4);
    return nimg;
}

cv::Mat ventana::dataImage(cv::Mat img, std::vector<cv::Point> centers)
{
    cv::Mat nimg;
    double zoom;
    nimg = cropImageProportion(img, centers,0.3445, 0.3457, 0.13,  0.87);
    return nimg;
}

void ventana::method1()
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx, centros, acta;
    vector<vector<cv::Point>> tempList, cuadros;
    vector<cv::Vec4i> hierarchy;
    cv::Mat canvas, mask;
    cv::Scalar color, cmean, stddev;
    cv::Mat imagen;
    int cont,i;
    QList<int> contornos;
    double area, areamin, areamax;

    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );
    cv::blur(imagen,imagen,cv::Size(5,5));
    cv::threshold(imagen,imagen,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    cv::Mat element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(imagen,imagen,cv::MORPH_CLOSE,element);
    /*element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(imagen,imagen,cv::MORPH_OPEN,element);*/

    cv::findContours(imagen,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    for(i=0;i < contours.size(); i++)
    {
        contornos.append(i);
    }

    areamin = mSrcImage.size().width*mSrcImage.size().height*0.00024;
    areamax = mSrcImage.size().width*mSrcImage.size().height*0.0034;

    qDebug() << "SizeIMG = ("<< mSrcImage.size().width << "," << mSrcImage.size().height << ")";
    qDebug() << "min=" << areamin << ", max=" << areamax;

    canvas = cv::Mat::zeros( mSrcImage.size(), CV_8UC3 );
    cont = 0;
    cuadros.clear();
    while (contornos.size() > 0)
    {
        for(i=contornos.first(); i >=0; i = hierarchy[i][0])
        {
            tempList.clear();
            cv::approxPolyDP(contours[i],approx,0.024*(cv::arcLength(contours[i],true)),true);
            tempList.push_back(approx);
            area = cv::contourArea(approx);
            if (approx.size() == 4  && cv::isContourConvex(approx) && area > areamin && area < areamax && almostSquare(approx) )
            {
                color = cv::Scalar(43,25,220);
                mask = cv::Mat::zeros( mSrcImage.size(), CV_8U );
                cv::drawContours(mask,tempList, 0, 255, -1);
                element = cv::getStructuringElement(0,cv::Size(6,6));
                cv::morphologyEx(mask,mask,cv::MORPH_ERODE,element);
                cmean = cv::mean(mCurrentImage,mask);
                cv::meanStdDev(mCurrentImage,cmean,stddev,mask);
                if (stdDevisOK(stddev) && meanisOK(cmean)){
                    qDebug() << "area=" << area;
                    color = cv::Scalar(25,220,62);
                    cuadros.push_back(approx);
                    cont++;
                }
                cv::drawContours( canvas, tempList, 0, color, 2, 8, hierarchy, 0, cv::Point() );
            }
            contornos.removeOne(i);
        }
    }
    if (cont == 3)
    {
        // Ordenamos los centros
        centros = orderCenters(getCenter(cuadros[0]), getCenter(cuadros[1]), getCenter(cuadros[2]));
        centros.push_back(complementaryPoint(centros[0], centros[1], centros[2]));
        acta = calcularActa(0.017,centros[0], centros[1], centros[2], centros[3]);
        color = cv::Scalar(255,126,0);
        tempList.clear();
        tempList.push_back(acta);
        cv::drawContours( canvas, tempList, 0, color, 2);
    }
    qDebug()<<"Contornos validos:" << cont <<" de: " << contours.size();

    showImage(canvas);

    commandsStructure accion;
    accion.setImage(canvas);
    accion.setMessage("method1");
    emit changeImage(accion);

}

void ventana::method2()
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx;
    vector<vector<cv::Point>> tempList;
    vector<cv::Vec4i> hierarchy;
    cv::Mat canvas;
    cv::Scalar color;
    cv::Mat imagen;
    int cont,i;
    QList<int> contornos;

    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );
    cv::GaussianBlur(imagen,imagen,cv::Size(5,5),0);
    cv::threshold(imagen,imagen,48,255,cv::THRESH_BINARY);
    cv::Mat element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(imagen,imagen,cv::MORPH_CLOSE,element);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(imagen,imagen,cv::MORPH_OPEN,element);


    cv::findContours(imagen,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    for(i=0;i < contours.size(); i++)
    {
        contornos.append(i);
    }

    canvas = cv::Mat::zeros( mSrcImage.size(), CV_8UC3 );
    cont = 0;
    while (contornos.size() > 0)
    {
        color= cv::Scalar( std::rand()%255, std::rand()%128, std::rand()%255 );
        for(i=contornos.first(); i >=0; i = hierarchy[i][0])
        {
            tempList.clear();
            cv::approxPolyDP(contours[i],approx,0.024*(cv::arcLength(contours[i],true)),true);
            tempList.push_back(approx);
            if (approx.size() == 4 && cv::contourArea(approx) > 1000)
            {
                cv::drawContours( canvas, tempList, 0, color, 2, 8, hierarchy, 0, cv::Point() );
                cont++;
            }
            contornos.removeOne(i);
        }
    }
    qDebug()<<"Contornos validos:" << cont <<" de: " << contours.size();

    showImage(canvas);

    commandsStructure accion;
    accion.setImage(canvas);
    accion.setMessage("method2");
    emit changeImage(accion);

}

void ventana::method3()
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx;
    vector<vector<cv::Point>> tempList;
    vector<cv::Vec4i> hierarchy;
    cv::Mat canvas;
    cv::Scalar color;
    cv::Mat imagen;
    int cont,i;
    QList<int> contornos;

    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );
    cv::GaussianBlur(imagen,imagen,cv::Size(5,5),0);
    cv::threshold(imagen,imagen,105,255,cv::THRESH_BINARY);
    cv::Mat element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(imagen,imagen,cv::MORPH_CLOSE,element);
    /*element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(imagen,imagen,cv::MORPH_OPEN,element);*/


    cv::findContours(imagen,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    for(i=0;i < contours.size(); i++)
    {
        contornos.append(i);
    }

    canvas = cv::Mat::zeros( mSrcImage.size(), CV_8UC3 );
    cont = 0;
    while (contornos.size() > 0)
    {
        color= cv::Scalar( std::rand()%255, std::rand()%128, std::rand()%255 );
        for(i=contornos.first(); i >=0; i = hierarchy[i][0])
        {
            tempList.clear();
            cv::approxPolyDP(contours[i],approx,0.024*(cv::arcLength(contours[i],true)),true);
            tempList.push_back(approx);
            if (approx.size() == 4 && cv::contourArea(approx) > 1000)
            {
                cv::drawContours( canvas, tempList, 0, color, 2, 8, hierarchy, 0, cv::Point() );
                cont++;
            }
            contornos.removeOne(i);
        }
    }
    qDebug()<<"Contornos validos:" << cont <<" de: " << contours.size();

    showImage(canvas);

    commandsStructure accion;
    accion.setImage(canvas);
    accion.setMessage("method3");
    emit changeImage(accion);

}

void ventana::UseMethod1()
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx, centros, acta;
    vector<vector<cv::Point>> tempList, cuadros;
    vector<cv::Vec4i> hierarchy;
    cv::Mat canvas,mask, pos, previewData, theData;
    cv::Scalar cmean, stddev;
    cv::Mat imagen;
    cv::RotatedRect actaFinal;
    cv::Point2f srcTri[3], destTri[3], srcF[4], destF[4], vertActa[4];
    cv::Size af_Size;
    int cont,i;
    double area, areamin, areamax;

    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );
    cv::blur(imagen,imagen,cv::Size(5,5));
    cv::threshold(imagen,imagen,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    cv::Mat element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(imagen,imagen,cv::MORPH_CLOSE,element);
    /*element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(imagen,imagen,cv::MORPH_OPEN,element);*/

    cv::findContours(imagen,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    areamin = mSrcImage.size().width*mSrcImage.size().height*0.00024;
    areamax = mSrcImage.size().width*mSrcImage.size().height*0.0034;

    qDebug() << "SizeIMG = ("<< mSrcImage.size().width << "," << mSrcImage.size().height << ")";
    qDebug() << "min=" << areamin << ", max=" << areamax;

    cont = 0;
    cuadros.clear();
    for(i=0; i < contours.size() ; i++)
    {
        tempList.clear();
        cv::approxPolyDP(contours[i],approx,0.024*(cv::arcLength(contours[i],true)),true);
        tempList.push_back(approx);
        area = cv::contourArea(approx);
        if (approx.size() == 4  && cv::isContourConvex(approx) && area > areamin && area < areamax && almostSquare(approx) )
        {
            mask = cv::Mat::zeros( mSrcImage.size(), CV_8U );
            cv::drawContours(mask,tempList, 0, 255, -1);
            element = cv::getStructuringElement(0,cv::Size(6,6));
            cv::morphologyEx(mask,mask,cv::MORPH_ERODE,element);
            cmean = cv::mean(mCurrentImage,mask);
            cv::meanStdDev(mCurrentImage,cmean,stddev,mask);
            if (stdDevisOK(stddev) && meanisOK(cmean)){
                qDebug() << "area=" << area;
                cuadros.push_back(approx);
                cont++;
            }
        }
    }
    if (cont == 3)
    {
        // Ordenamos los centros
        centros = orderCenters(getCenter(cuadros[0]), getCenter(cuadros[1]), getCenter(cuadros[2]));

        acta = calculatePerspectiveActa(cuadros,centros,mSrcImage);
        if (acta.size() == 0)
        {
            centros.push_back(complementaryPoint(centros[0], centros[1], centros[2]));

            acta = calcularActa(0.017,centros[0], centros[1], centros[2], centros[3]);
            actaFinal = cv::minAreaRect(acta);
            actaFinal.points(vertActa);
            calcTransformationsPoints(vertActa,centros,srcTri,destTri);
            pos = cv::getAffineTransform(srcTri,destTri);

            af_Size.width = cv::norm(destTri[0] - destTri[1]);
            af_Size.height = cv::norm(destTri[0] - destTri[2]);

            cv::warpAffine(mCurrentImage,canvas,pos,af_Size);

            cuadros = reCalculateSquares(cuadros, pos);
            centros = reCalculateCenters(centros,pos);
        }
        else
        {
            for(i=0; i < acta.size(); i++)
            {
                vertActa[i] = acta.at(i);
            }
            calcTransformationsPoints(vertActa,centros,srcF,destF,4);
            pos = cv::getPerspectiveTransform(srcF,destF);

            af_Size.width = cv::norm(destF[0] - destF[1]);
            af_Size.height = cv::norm(destF[0] - destF[2]);

            cv::warpPerspective(mCurrentImage,canvas,pos,af_Size);
            cuadros = reCalculateSquaresP(cuadros, pos);
            centros = reCalculateCentersP(centros,pos);
        }



        previewData = previewImage(canvas,centros);
        theData = dataImage(canvas,centros);

        showImage(theData);


    }
    qDebug()<<"Contornos validos:" << cont <<" de: " << contours.size();

    //showImage(canvas);

    commandsStructure accion;
    accion.setImage(theData);
    accion.setMessage("Use method1");
    emit changeImage(accion);
}

void ventana::UseMethodsCombined()
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> centros, acta;
    vector<vector<cv::Point>> cuadros, temp;
    cv::Mat canvas, pos, previewData, theData, base, output, element;
    cv::RotatedRect actaFinal;
    cv::Point2f srcTri[3], destTri[3], vertActa[4];
    cv::Size af_Size;
    int cont,i, idx;

    cv::cvtColor( mCurrentImage, base, CV_BGR2GRAY );
    cv::blur(base,base,cv::Size(5,5));
    cv::threshold(base,output,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    cuadros.clear();
    temp = obtainSquares(output, mCurrentImage);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    cv::threshold(base,output,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, mCurrentImage);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    cv::threshold(base,output,122,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, mCurrentImage);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    cv::threshold(base,output,48,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);

    temp = obtainSquares(output, mCurrentImage);
    cuadros.insert(cuadros.end(),temp.begin(),temp.end());

    removeDuplicateSquares(&cuadros);

    if (cuadros.size() > 3)
    {
        temp.clear();
        for(i=0; i < 3; i++)
        {
            idx = minimumStdDevSquares(cuadros,mCurrentImage);

            temp.push_back(cuadros[idx]);
            cuadros.erase(cuadros.begin()+idx);
        }
    }
    else
    {
        temp.clear();
        temp = cuadros;
    }




    if (temp.size() == 3)
    {
        cuadros = temp;
        // Ordenamos los centros
        centros = orderCenters(getCenter(cuadros[0]), getCenter(cuadros[1]), getCenter(cuadros[2]));

        centros.push_back(complementaryPoint(centros[0], centros[1], centros[2]));

        acta = calcularActa(0.017,centros[0], centros[1], centros[2], centros[3]);
        actaFinal = cv::minAreaRect(acta);
        actaFinal.points(vertActa);
        calcTransformationsPoints(vertActa,centros,srcTri,destTri);
        pos = cv::getAffineTransform(srcTri,destTri);

        af_Size.width = cv::norm(destTri[0] - destTri[1]);
        af_Size.height = cv::norm(destTri[0] - destTri[2]);

        cv::warpAffine(mCurrentImage,canvas,pos,af_Size);

        cuadros = reCalculateSquares(cuadros, pos);
        centros = reCalculateCenters(centros,pos);

        /*previewData = previewImage(canvas,centros);
        theData = dataImage(canvas,centros);

        showImage(canvas);*/


    }
    qDebug()<<"Contornos validos:" << cont <<" de: " << contours.size();

    showImage(canvas);

    commandsStructure accion;
    accion.setImage(canvas);
    accion.setMessage("Use method48outsu10548");
    emit changeImage(accion);

}

void ventana::combineThreshold()
{
    cv::Mat imgBlur, threshold1, threshold2, thresholdfinal, element;
    cv::cvtColor( mCurrentImage, imgBlur, CV_BGR2GRAY );
    cv::GaussianBlur(imgBlur,imgBlur,cv::Size(5,5),0);

    cv::threshold(imgBlur,threshold1,48,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(threshold1,threshold1,cv::MORPH_CLOSE,element);

    cv::threshold(imgBlur,threshold2,105,255,cv::THRESH_BINARY);
    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(threshold2,threshold2,cv::MORPH_OPEN,element,cv::Point(-1,-1),2);

    thresholdfinal = ThresholdingCombiner(threshold1,threshold2);

    element = cv::getStructuringElement(0,cv::Size(6,6));
    cv::morphologyEx(thresholdfinal,thresholdfinal,cv::MORPH_CLOSE,element);

    cv::cvtColor( thresholdfinal, thresholdfinal, CV_GRAY2BGR );

    showImage(thresholdfinal);

    commandsStructure accion;
    accion.setImage(thresholdfinal);
    accion.setMessage("combineThresholds");
    emit changeImage(accion);
}

void ventana::HoughLinesP(double rho, double theta, int threshold, double minLineLength, double maxLineGap)
{
    vector<cv::Vec4i> lines;
    cv::Mat canvas,imagen;
    canvas = cv::Mat::zeros( mCurrentImage.size(), CV_8UC3 );

    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );
    cv::HoughLinesP(imagen,lines,rho,theta,threshold,minLineLength,maxLineGap);
    for( int i = 0; i < lines.size(); i++ )
    {
        line( canvas, cv::Point(lines[i][0], lines[i][1]),cv::Point(lines[i][2], lines[i][3]), cv::Scalar(0,0,255), 3, 8 );
    }
    showImage(canvas);

    commandsStructure accion;
    accion.setImage(canvas);
    accion.setMessage("HLP-rho:"+QString::number(rho)+"-theta:"+QString::number(theta)+"-threshold:"+
                      QString::number(threshold)+"-minL:"+QString::number(minLineLength)+"-maxL:"+
                      QString::number(maxLineGap));
    emit changeImage(accion);
}

void ventana::Sobel(int ddepth, int dx, int dy, int ksize, double scale, double delta, int borderType)
{
    cv::Mat imagen;
    cv::Mat grad;

    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );

    cv::Sobel( imagen, grad, ddepth, dx, dy, ksize, scale, delta, bordes(borderType) );

    cv::convertScaleAbs( grad, imagen );
    cv::cvtColor(imagen,imagen,CV_GRAY2BGR);

    showImage(imagen);

    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("Sobel-ddepth:"+QString::number(ddepth)+"-dx:"+QString::number(dx)+"-dy:"+
                      QString::number(dy)+"-ksize:"+QString::number(ksize)+"-scale:"+
                      QString::number(scale)+QString::number(ksize)+"-delta:"+
                      QString::number(delta)+"-border:"+QString::number(borderType));

    emit changeImage(accion);


}

void ventana::Scharr(int ddepth, int dx, int dy, double scale, double delta, int borderType)
{
    cv::Mat imagen;
    cv::Mat grad;

    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );

    cv::Scharr( imagen, grad, ddepth, dx, dy, scale, delta, bordes(borderType) );

    cv::convertScaleAbs( grad, imagen );
    cv::cvtColor(imagen,imagen,CV_GRAY2BGR);

    showImage(imagen);

    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("Scharr-ddepth:"+QString::number(ddepth)+"-dx:"+QString::number(dx)+"-dy:"+
                      QString::number(dy)+"-scale:"+ QString::number(scale)+"-delta:"+
                      QString::number(delta)+"-border:"+QString::number(borderType));

    emit changeImage(accion);
}

void ventana::SobelCombined()
{
    cv::Mat imagen;
    cv::Mat grad_x, grad_y, abs_grad_x, abs_grad_y;

    cv::cvtColor( mCurrentImage, imagen, CV_BGR2GRAY );

    cv::Sobel( imagen, grad_x, -1, 2, 0, 3, 1, 0, cv::BORDER_DEFAULT);
    cv::Sobel( imagen, grad_y, -1, 0, 2, 3, 1, 0, cv::BORDER_DEFAULT);

    cv::convertScaleAbs( grad_x, abs_grad_x );
    cv::convertScaleAbs( grad_y, abs_grad_y );

    cv::addWeighted( abs_grad_x, 0.5, abs_grad_y, 0.5, 0, imagen );


    cv::cvtColor(imagen,imagen,CV_GRAY2BGR);

    showImage(imagen);

    commandsStructure accion;
    accion.setImage(imagen);
    accion.setMessage("Sobel Combinado");
    emit changeImage(accion);
}

int ventana::bordes(int i)
{
    int tipo;
    switch (i) {
    case 0:
        tipo = cv::BORDER_CONSTANT;
        break;
    case 1:
        tipo = cv::BORDER_REPLICATE;
        break;
    case 2:
        tipo = cv::BORDER_REFLECT;
        break;
    case 3:
        tipo = cv::BORDER_WRAP;
        break;
    case 4:
        tipo = cv::BORDER_REFLECT_101;
        break;
    case 5:
        tipo = cv::BORDER_TRANSPARENT;
        break;
    case 6:
        tipo = cv::BORDER_ISOLATED;
        break;
    default:
        tipo = cv::BORDER_DEFAULT;
        break;
    }

    return tipo;
}

int ventana::threshtypes(int i)
{
    int tipo;
    switch (i) {
    case 0:
        tipo = cv::THRESH_BINARY;
        break;
    case 1:
        tipo = cv::THRESH_BINARY_INV;
        break;
    case 2:
        tipo = cv::THRESH_TRUNC;
        break;
    case 3:
        tipo = cv::THRESH_TOZERO;
        break;
    case 4:
        tipo = cv::THRESH_TOZERO_INV;
        break;
    case 5:
        tipo = cv::THRESH_MASK;
        break;
    case 6:
        tipo = cv::THRESH_OTSU;
        break;
    /*case 7:
        tipo = cv::THRESH_TRIANGLE;
        break;*/
    case 8:
        tipo = cv::THRESH_OTSU+cv::THRESH_BINARY;
        break;
    case 9:
        tipo = cv::THRESH_OTSU+cv::THRESH_BINARY_INV;
        break;
    case 10:
        tipo = cv::THRESH_OTSU+cv::THRESH_TRUNC;
        break;
    case 11:
        tipo = cv::THRESH_OTSU+ cv::THRESH_TOZERO;
        break;
    case 12:
        tipo = cv::THRESH_OTSU+cv::THRESH_TOZERO_INV;
        break;
    case 13:
        tipo = cv::THRESH_OTSU+cv::THRESH_MASK;
        break;
    /*case 14:
        tipo = cv::THRESH_TRIANGLE+cv::THRESH_BINARY;
        break;
    case 15:
        tipo = cv::THRESH_TRIANGLE+cv::THRESH_BINARY_INV;
        break;
    case 16:
        tipo = cv::THRESH_TRIANGLE+cv::THRESH_TRUNC;
        break;
    case 17:
        tipo = cv::THRESH_TRIANGLE+ cv::THRESH_TOZERO;
        break;
    case 18:
        tipo = cv::THRESH_TRIANGLE+cv::THRESH_TOZERO_INV;
        break;
    case 19:
        tipo = cv::THRESH_TRIANGLE+cv::THRESH_MASK;
        break;*/
    default:
        tipo = cv::THRESH_BINARY;
        break;
    }
    return tipo;
}

void ventana::removeDuplicateSquares(std::vector<std::vector<cv::Point>> *quads)
{
    std::vector<cv::Point> centros, lado;
    std::vector<double> lados;

    int i ,j;

    for(i=0; i < quads->size(); i++)
    {
        centros.push_back(getCenter(quads->at(i)));
        lado.clear();
        lado.push_back((quads->at(i))[0]);
        lado.push_back((quads->at(i))[1]);
        lados.push_back(cv::arcLength(lado,false));
    }

    i=0;
    while(i < quads->size())
    {
        j=i+1;
        while(j < quads->size())
        {
            lado.clear();
            lado.push_back(centros.at(i));
            lado.push_back(centros.at(j));
            if (cv::arcLength(lado,false) < 2*lados.at(i))
            {
                lados.erase(lados.begin()+j);
                centros.erase(centros.begin()+j);
                quads->erase(quads->begin()+j);
            }
            else
            {
                j++;
            }
        }
        i++;
    }
}

vector<vector<cv::Point>> ventana::obtainSquares(cv::Mat imagen, cv::Mat srcimg)
{
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx;
    vector<vector<cv::Point>> tempList, cuadros;
    vector<cv::Vec4i> hierarchy;
    cv::Mat mask, element;
    cv::Scalar cmean, stddev;
    cv::Scalar color;
    int i;
    double area, areamin, areamax;

    cv::findContours(imagen,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

    areamin = imagen.size().width*imagen.size().height*0.00024;
    areamax = imagen.size().width*imagen.size().height*0.0034;

    cuadros.clear();
    for(i=0; i < contours.size() ; i++)
    {
        tempList.clear();
        cv::approxPolyDP(contours[i],approx,0.025*(cv::arcLength(contours[i],true)),true);
        tempList.push_back(approx);
        area = cv::contourArea(approx);
        if (approx.size() == 4  && cv::isContourConvex(approx) && area > areamin && area < areamax && almostSquare(approx) )
        {
            color = cv::Scalar(43,25,220);
            mask = cv::Mat::zeros( srcimg.size(), CV_8U );
            cv::drawContours(mask,tempList, 0, 255, -1);
            element = cv::getStructuringElement(0,cv::Size(6,6));
            cv::morphologyEx(mask,mask,cv::MORPH_ERODE,element);
            cmean = cv::mean(srcimg,mask);
            cv::meanStdDev(srcimg,cmean,stddev,mask);
            if (stdDevisOK(stddev) && meanisOK(cmean)){
                //qDebug() << "area=" << area;
                cuadros.push_back(approx);

                color = cv::Scalar(25,220,62);
            }
            cv::drawContours( srcimg, tempList, 0, color, 3, 8, hierarchy, 0, cv::Point() );
        }
    }

    return cuadros;
}

int ventana::minimumStdDevSquares(std::vector<std::vector<cv::Point>> &cuadros, cv::Mat srcimg)
{
    int i, imin=-1;
    cv::Mat mask, element;
    cv::Scalar cmean, stddev;
    double dev, devmin=1e10;

    for(i=0; i < cuadros.size() ; i++)
    {
        mask = cv::Mat::zeros( srcimg.size(), CV_8U );
        cv::drawContours(mask,cuadros, i, 255, -1);
        element = cv::getStructuringElement(0,cv::Size(6,6));
        cv::morphologyEx(mask,mask,cv::MORPH_ERODE,element);
        cmean = cv::mean(srcimg,mask);
        cv::meanStdDev(srcimg,cmean,stddev,mask);
        dev = stddev[0]+stddev[1]+stddev[2];
        if (dev < devmin)
        {
            devmin = dev;
            imin = i;
        }
    }

    return imin;
}

/** Dibujado de la imagen **/

ventana::ventana(QWidget *parent)
    : QOpenGLWidget(parent)
{
    mBgColor = QColor::fromRgb(150, 150, 150);
}

ventana::~ventana(){}


void ventana::initializeGL()
{
    makeCurrent();
    initializeOpenGLFunctions();

    float r = ((float)mBgColor.darker().red())/255.0f;
    float g = ((float)mBgColor.darker().green())/255.0f;
    float b = ((float)mBgColor.darker().blue())/255.0f;
    glClearColor(r,g,b,1.0f);
}

void ventana::resizeGL(int width, int height)
{
    makeCurrent();
    glViewport(0, 0, (GLint)width, (GLint)height);

    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    glOrtho(0, width, -height, 0, 0, 1);

    glMatrixMode(GL_MODELVIEW);

    recalculatePosition();

    emit imageSizeChanged(mRenderWidth, mRenderHeight);

    updateScene();
}

void ventana::updateScene(){ if (this->isVisible()) update(); }

void ventana::paintGL()
{
    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    renderImage();
}

void ventana::renderImage()
{
    makeCurrent();

    glClear(GL_COLOR_BUFFER_BIT);

    if (!mRenderQtImg.isNull())
    {
        glLoadIdentity();

        glPushMatrix();
        {
            if (mResizedImg.width() <= 0)
            {
                if (mRenderWidth == mRenderQtImg.width() && mRenderHeight == mRenderQtImg.height())
                    mResizedImg = mRenderQtImg;
                else
                    mResizedImg = mRenderQtImg.scaled(QSize(mRenderWidth, mRenderHeight),
                                                      Qt::IgnoreAspectRatio,
                                                      Qt::SmoothTransformation);
            }

            // ---> Centering image in draw area

            glRasterPos2i(mRenderPosX, mRenderPosY);

            glPixelZoom(1, -1);

            glDrawPixels(mResizedImg.width(), mResizedImg.height(), GL_RGBA, GL_UNSIGNED_BYTE, mResizedImg.bits());
        }
        glPopMatrix();

        // end
        glFlush();
    }
}


void ventana::recalculatePosition()
{
    mImgRatio = (float)mRenderImage.cols/(float)mRenderImage.rows;

    mRenderWidth = this->size().width();
    mRenderHeight = floor(mRenderWidth / mImgRatio);

    if (mRenderHeight > this->size().height())
    {
        mRenderHeight = this->size().height();
        mRenderWidth = floor(mRenderHeight * mImgRatio);
    }

    mRenderPosX = floor((this->size().width() - mRenderWidth) / 2);
    mRenderPosY = -floor((this->size().height() - mRenderHeight) / 2);

    mResizedImg = QImage();
}

bool ventana::showImage(const cv::Mat& image)
{
    mCurrentImage = image;
    if (image.channels() == 3)
        cv::cvtColor(image, mRenderImage, CV_BGR2RGBA);
    else if (image.channels() == 1)
        cv::cvtColor(image, mRenderImage, CV_GRAY2RGBA);
    else
    {
        qDebug()<<"Canales=" << image.channels();
        return false;
    }

    mRenderQtImg = QImage((const unsigned char*)(mRenderImage.data),
                          mRenderImage.cols, mRenderImage.rows,
                          mRenderImage.step1(), QImage::Format_RGB32);

    recalculatePosition();

    updateScene();

    return true;
}

bool ventana::loadImage(const cv::Mat& image)
{
    mSrcImage = image.clone();
    return showImage(image);
}

void ventana::back2Source()
{
    showImage(mSrcImage);
}

cv::Scalar ventana::LinearGradient(cv::Scalar A, cv::Scalar B, float pos)
{
    cv::Scalar salida;
    salida[0] = A[0] + (int)(pos*(B[0] - A[0]));
    salida[1] = A[1] + (int)(pos*(B[1] - A[1]));
    salida[2] = A[2] + (int)(pos*(B[2] - A[2]));
    salida[3] = A[3] + (int)(pos*(B[3] - A[3]));

    return salida;
}

QString ventana::Color2String(QColor c)
{
    return "(r:"+QString::number(c.red())+",g:"+QString::number(c.green())+",b:"+QString::number(c.blue())+")";
}

cv::Mat ventana::ThresholdingCombiner(cv::Mat img1, cv::Mat img2){
    int i,j;
    cv::Mat imgf = img1.clone();

    int cols = img1.cols, rows = img1.rows;
    if(img1.isContinuous())
    {
        cols *= rows;
        rows = 1;
    }

    for (i=0; i < rows; i++){

        const uchar* M1i = img1.ptr<uchar>(i);
        const uchar* M2i = img2.ptr<uchar>(i);
        uchar* Mfi = imgf.ptr<uchar>(i);

        for(j=0; j < cols; j++){
            Mfi[j] = combine(M1i[j],M2i[j]);
        }
    }

    return imgf;

}

uchar ventana::combine(uchar color1, uchar color2){
    if (color1 == color2 ){
        return color1;
    }
    else{
        return 255;
    }

}

void ventana::saveImg(cv::Mat img,QString name)
{
    cv::Mat nimage;
    if (img.channels() == 3)
        cv::cvtColor(img, nimage, CV_BGR2RGB);
    else if (img.channels() == 1)
        cv::cvtColor(img, nimage, CV_GRAY2RGB);

    QImage imagen = QImage((const unsigned char*)(nimage.data),nimage.cols, nimage.rows,nimage.step1(), QImage::Format_RGB888);
    imagen.save(name);
}

cv::Point ventana::calculatePerspectiveFourthPoint(std::vector<std::vector<cv::Point>> squares, std::vector<cv::Point> centers)
{
    cv::Point2d centro, A1, A2, B1, B2, lado, lado2, lado3, c1, c2;
    double cos1, cos2, alpha=0,tan=0, div=0;
    std::vector<cv::Point> cuadrado;
    bool bien=false;
    int idx[3],i,j;

    //buscamos los cuadrados ordenados;
    for(i=1; i < centers.size(); i++)
    {
        for(j=0; j < squares.size(); j++)
        {
            centro = getCenter(squares[j]);
            if (centro.x == centers[i].x && centro.y == centers[i].y)
                idx[i] = j;
        }
    }

    lado = centers[1]-centers[0];
    cuadrado = squares[idx[1]];

    lado2 = cuadrado[1]-cuadrado[0];
    lado3 = cuadrado[3]-cuadrado[0];
    cos1 = std::abs(lado.dot(lado2)/(cv::norm(cv::Mat(lado2))*cv::norm(cv::Mat(lado))));
    cos2 = std::abs(lado.dot(lado3)/(cv::norm(cv::Mat(lado3))*cv::norm(cv::Mat(lado))));
    c1 = cuadrado[2];
    if (cos1 < cos2)
    {
        c2 = cuadrado[3];
        A2 = lado2 + (c1-c2);
    }
    else
    {
        c2 = cuadrado[1];
        A2 = lado3 + (c1-c2);
    }
    A1 = centers[1];
    A2.x = A2.x/2;
    A2.y = A2.y/2;

    lado = centers[2]-centers[0];
    cuadrado = squares[idx[2]];

    lado2 = cuadrado[1]-cuadrado[0];
    lado3 = cuadrado[3]-cuadrado[0];
    cos1 = std::abs(lado.dot(lado2)/(cv::norm(cv::Mat(lado2))*cv::norm(cv::Mat(lado))));
    cos2 = std::abs(lado.dot(lado3)/(cv::norm(cv::Mat(lado3))*cv::norm(cv::Mat(lado))));
    c1 = cuadrado[2];
    if (cos1 < cos2)
    {
        c2 = cuadrado[3];
        B2 = lado2 + (c1-c2);
    }
    else
    {
        c2 = cuadrado[1];
        B2 = lado3 + (c1-c2);
    }
    B1 = centers[2];
    B2.x = B2.x/2;
    B2.y = B2.y/2;

    if (std::abs(A2.x+A2.y) > 1e-16 || std::abs(B2.x+B2.y) > 1e-16)
    {
        if (std::abs(B2.x) > 1e-16)
        {
            tan = B2.y/B2.x;
            div = A2.y - A2.x*tan;
            if (std::abs(div) > 1e-16)
            {
                alpha = (B1.y-A1.y - (B1.x-A1.x)*tan)/div;
                bien = true;
            }
        }
        else
        {
            tan = B2.x/B2.y;
            div = (A2.x - A2.y*tan);
            if (std::abs(div) > 1e-16)
            {
                alpha = (B1.x-A1.x - (B1.y - A1.y)*tan)/div;
                bien = true;
            }
        }
    }

    if (bien)
    {
        centro = A1 + alpha*A2;
    }
    else
    {
        centro = centers[0];
    }

    return centro;

}

std::vector<cv::Point> ventana::calculatePerspectiveActa(std::vector<std::vector<cv::Point>> squares, std::vector<cv::Point> &centers, cv::Mat img)
{
    std::vector<cv::Point> acta, actamin;
    vector<vector<cv::Point>> contours;
    vector<cv::Point> approx;
    vector<cv::Vec4i> hierarchy;
    double area1, areamin, area;
    cv::Mat output, element;
    int i;

    cv::Point centro = calculatePerspectiveFourthPoint(squares,centers);

    if (centro.x != centers[0].x || centro.y != centers[0].y)
    {
        centers.push_back(centro);
        acta = calcularActa(0.017,centers[0], centers[1], centers[2], centers[3]);

        area1 = cv::contourArea(acta);
        qDebug() << "Area O: " << area1;

        cv::cvtColor( img, output, CV_BGR2GRAY );
        cv::blur(output,output,cv::Size(5,5));
        cv::threshold(output,output,70,255,cv::THRESH_BINARY + cv::THRESH_OTSU);
        element = cv::getStructuringElement(0,cv::Size(6,6));
        cv::morphologyEx(output,output,cv::MORPH_CLOSE,element);
        cv::findContours(output,contours,hierarchy,CV_RETR_TREE,CV_CHAIN_APPROX_SIMPLE);

        areamin = -1;
        for(i=0; i < contours.size() ; i++)
        {
            cv::approxPolyDP(contours[i],approx,0.025*(cv::arcLength(contours[i],true)),true);
            area = cv::contourArea(approx);
            if (approx.size() == 4)
                qDebug() << "Area "  << i << " : " << area;
            if (approx.size() == 4 && std::abs(area - area1) < 0.1*area1)
            {
                if (areamin < 0)
                {
                    areamin = area;
                    actamin = approx;
                }
                else
                {
                    if (area < areamin)
                    {
                        areamin = area;
                        actamin = approx;
                    }
                }
            }
        }
        if (areamin > 0)
        {
            acta = actamin;
        }

    }

    return acta;
}
