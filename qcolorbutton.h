#ifndef QCOLORBUTTON_H
#define QCOLORBUTTON_H

#include <QObject>
#include <QPushButton>
#include <QColorDialog>
#include <QColor>
#include <QRect>
#include <QPainter>
#include <QPaintEvent>

class QColorButton : public QPushButton
{
     Q_OBJECT
public:
    explicit QColorButton(const QColor & color = Qt::black, QWidget *parent = 0);
    QColor getColor() const{return currentColor;}
signals:
    void colorChanged(QColor);

public slots:
    void changeColor(const QColor &);
    void chooseColor();
    void paintEvent(QPaintEvent *event);

private:
    QColor currentColor;
};

#endif // QCOLORBUTTON_H
