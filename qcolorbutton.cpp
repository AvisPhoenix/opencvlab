#include "qcolorbutton.h"

QColorButton::QColorButton(const QColor & color, QWidget *parent) : QPushButton(parent)
{
    this->setMinimumWidth(50);
    currentColor = color;
    connect(this, SIGNAL(clicked()), this, SLOT(chooseColor()));
}

void QColorButton::changeColor(const QColor & color)
{
    currentColor = color;
    colorChanged(currentColor);
}

void QColorButton::chooseColor()
{
    QColor color = QColorDialog::getColor(currentColor, this);
    if (color.isValid())
        changeColor(color);
}

void QColorButton::paintEvent(QPaintEvent *event)
{
    QPushButton::paintEvent(event);

    int colorPadding = 5;

    QRect rect = event->rect();
    QPainter painter( this );
    painter.setBrush( QBrush( currentColor ) );
    painter.setPen("#CECECE");
    rect.adjust(colorPadding, colorPadding, -1-colorPadding, -1-colorPadding);
    painter.drawRect(rect);
}
